var fs = require ("fs");
const resObjectPlain={};
const resObjectManyLevel={};
// fs.readFile('./translation.json', "utf8", (err, fileContents)=>{
//   if (err){
//     console.log(err);
//     return
//   }
//   try {
//     const sourceObject = JSON.parse(fileContents)
//     resObject = sourceObject;
//   }
//   catch(err){
//   console.log(err);
// }
// })

var sourceObject = require("./translation.json");

// ===Plain JSON===
for (let key in sourceObject) {
  let currentString = sourceObject[key];
  if (~currentString.indexOf("\n")) {
    let index = 1;
    while (~currentString.indexOf("\n")) {
      let str = currentString.slice(0, currentString.indexOf("\n"));
      let newKey = key + '---' + index;
      resObjectPlain[newKey] = str.trim();
      currentString = currentString.substring(currentString.indexOf("\n") + 1).trim();
      index++
    }
    resObjectPlain[key + '---' + index] = currentString;
  } else {
    resObjectPlain[key] = currentString;
  }
}

// ===== Many Level JSON =======
for (let key in sourceObject) {
  let currentString = sourceObject[key];
  if (~currentString.indexOf("\n")) {
    resObjectManyLevel[key]={};
    newLvl = resObjectManyLevel[key];
    let index = 1;
    while (~currentString.indexOf("\n")) {
      let str = currentString.slice(0, currentString.indexOf("\n"));
      let newKey = 'p-' + index;
      newLvl[newKey] = str.trim();
      currentString = currentString.substring(currentString.indexOf("\n") + 1).trim();
      index++
    }
    newLvl['p-' + index] = currentString;
  } else {
    resObjectManyLevel[key] = currentString;
  }
}


fs.writeFile("./result-plain.json", JSON.stringify(resObjectPlain), (err) => {
  if (err) {
    console.log (err);
    return;
  };
  console.log("ok, created");
});

fs.writeFile("./result-ml.json", JSON.stringify(resObjectManyLevel), (err) => {
  if (err) {
    console.log (err);
    return;
  };
  console.log("ok, created");
});